package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

import java.util.ArrayList;
import java.util.List;

@Component
public class CreateMovieMapper {

    public Movie mapToMovie(CreateMovieDto dto) {
        Movie returnedMovie = new Movie();

        returnedMovie.setTitle(dto.getTitle());
        returnedMovie.setImage(dto.getImage());
        returnedMovie.setYear(dto.getYear());

        return returnedMovie;
    }

}
